<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function index(Request $request) {

        $comments = Comment::where(['commentable_id'=> $request->id, 'commentable_type'=> $request->type])->get();


        return response()->json($comments);
    }
}
