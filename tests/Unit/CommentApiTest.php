<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Comment;
use App\Models\Post;

use Illuminate\Support\Facades\Artisan;
class CommentsApiTest extends TestCase
{

    public function setUp() {

        parent::setUp();
        Artisan::call('migrate');
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetComments()
    {
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->create(['commentable_type' => 'POST', 'commentable_id' => $post->id]);
        $comment2 = factory(Comment::class)->create(['commentable_type' => 'POST', 'commentable_id' => $post->id]);
        $comment3 = factory(Comment::class)->create(['commentable_type' => 'POST', 'commentable_id' => $post->id]);
        $response = $this->call('GET', '/comments', ['type' => 'POST', 'id' => $post->id]);
        $comments = json_decode($response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(3, count($comments));

    }
}
